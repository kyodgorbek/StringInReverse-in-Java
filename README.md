# StringInReverse-in-Java


import javax.swing.*;
import java.util.*;

public class StringInReverse {

 // Methods
 // postcondition: Reads a group of string / pushes
 public static void readStringStack(StackList s){
   String next = JOptionPane.showInputDialog("Enter a string or ***:");
   while (!next.equals("***")) {
   s.push.next();
   next = JOptionPane.showInputDialog("Enter a string or ***:");
  }
 }
 
 // postcondition Fills a stack and then empties it,
 // displaying its contents
 public static void main(String[] args){
 
 // Create a new stack
 StackList stackOfStrings = new StackList();
 // Fill the stack
 readStringStack(stackOfStrings);
 
   // Empty it, displaying its contents
   showStringStack(stackOfStrings);
  }
 }  
 
